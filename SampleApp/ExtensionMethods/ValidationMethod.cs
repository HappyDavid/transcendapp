﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.ExtensionMethods
{
    public static class ValidationMethod
    {
        public static bool CanAuthenticateToAppKey(this String AppKey)
        {
            try
            {
                if (AppKey == null)
                {
                    throw new ArgumentNullException("AppKey");
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static bool CanAuthenticateToAppSecret(this String AppSecret)
        {
            try
            {
                if (AppSecret == null)
                {
                    throw new ArgumentNullException("AppSecret");
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
