﻿using System;
using System.Windows;
using System.Configuration;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using SampleApp.Models;
using SampleApp.Component;
using SampleApp.Component.Impl;

namespace SampleApp
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _xmlUri = ConfigurationManager.AppSettings["XmlUrl"];
        private string _appKey = ConfigurationManager.AppSettings["DropboxAppKey"];
        private string strAccessToken = string.Empty;
        private string strAuthenticationURL = string.Empty;
        private readonly IDropboxAuthenticate  _dropboxAuthenticate;
        private DropBoxBase dropBoxBase;

        public MainWindow(
            DropboxAuthenticate dropboxAuthenticate
            )
        {
            InitializeComponent();
            _dropboxAuthenticate = dropboxAuthenticate;
        }

        private void TokenText_GotFocus(object sender, RoutedEventArgs e)
        {
            if (TokenText.Tag.ToString() == "True")
            {
                TokenText.Tag = "False";
                TokenText.Text = "";
            }
        }

        private void TokenText_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TokenText.Text) && TokenText.Tag.ToString() == "False")
            {
                TokenText.Text = "Enter user secret here";
                TokenText.Tag = "True";
            }
        }

        private async void GetInfo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(_xmlUri))
                {
                    ErrorMsg.Text = "XmlUri can not be null";
                    return;
                }

                XmlGet xml = new XmlGet();
                var productInfo = await xml.get(_xmlUri);

                SN.Text = $"SN : {productInfo.SN}";
                PN.Text = $"PN : {productInfo.PN}";
                IO.Text = $"IO : {productInfo.IO}";
                FW.Text = $"FW : {productInfo.FW}";
            }
            catch (Exception ex)
            {
                ErrorMsg.Text = $"{ex.ToString()}";
            }
        }

        private async void SignIn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(_appKey))
                {
                    MessageBox.Show("Please enter valid App Key !");
                    return;
                }
                if (dropBoxBase == null || string.IsNullOrEmpty(dropBoxBase.AccessTocken))
                {
                    dropBoxBase = new DropBoxBase(_appKey, "SampleApp");
                    var accountnfo = await _dropboxAuthenticate.Authenticate(_appKey, dropBoxBase);

                    AccountName.Text = $"{accountnfo.Name}";
                    AccountEmail.Text = $"{accountnfo.Email}";
                    if (!string.IsNullOrEmpty(accountnfo.PhotoUrl))
                    {
                        BitmapImage bitimg = new BitmapImage(new Uri(accountnfo.PhotoUrl, UriKind.Absolute));
                        AccountPhoto.Source = bitimg as ImageSource;
                        AccountPhoto.Width = 50;
                        AccountPhoto.Height = 50;
                        AccountPhoto.Visibility = Visibility.Visible;
                        DefaultPhoto.Visibility = Visibility.Collapsed;
                    }
                    SignIn.Visibility = Visibility.Collapsed;
                    SignOut.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
        }

        private void SignOut_Click(object sender, RoutedEventArgs e)
        {
            if (dropBoxBase != null)
            {
                dropBoxBase.Logout();

                AccountName.Text = "Name";
                AccountEmail.Text = "Email";
                DefaultPhoto.Visibility = Visibility.Visible;
                AccountPhoto.Visibility = Visibility.Collapsed;
                AccountPhoto.Source = null;
                SignIn.Visibility = Visibility.Visible;
                SignOut.Visibility = Visibility.Collapsed;
            }
        }
    }
}
