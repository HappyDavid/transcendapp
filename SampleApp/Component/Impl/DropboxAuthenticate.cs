﻿using SampleApp.Infra.Models;
using SampleApp.Models;
using System;
using System.Threading.Tasks;

namespace SampleApp.Component.Impl
{
    public class DropboxAuthenticate : IDropboxAuthenticate
    {
        public async Task<CurrentAccountInfo> Authenticate(string AppKey, DropBoxBase dropBoxBase)
        {
            try
            {
                var strAuthenticationURL = dropBoxBase.GeneratedAuthenticationURL();
                var strAccessToken = dropBoxBase.GenerateAccessToken();
                var accountnfo = await dropBoxBase.GetCurrentAccount(strAccessToken);

                return accountnfo;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
    }
}
