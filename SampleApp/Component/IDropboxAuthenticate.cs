﻿using SampleApp.Models;
using SampleApp.Infra.Models;
using System.Threading.Tasks;

namespace SampleApp.Component
{
    public interface IDropboxAuthenticate
    {
        Task<CurrentAccountInfo> Authenticate(string AppKey, DropBoxBase dropBoxBase);
    }
}
