﻿using Unity;
using System.Windows;
using SampleApp.Component;
using SampleApp.Component.Impl;

namespace SampleApp
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            IUnityContainer container = new UnityContainer();
            container.RegisterType<IDropboxAuthenticate, DropboxAuthenticate>();

            var window = container.Resolve<MainWindow>();
            window.Show();
        }
    }
}
