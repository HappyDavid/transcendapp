﻿using SampleApp.Infra.Helpers;
using System;
using System.Threading.Tasks;
using SampleApp.Infra.Models;
using SampleApp.Infra.ExtensionMethods;

namespace SampleApp.Models
{
    class XmlGet
    {
        public async Task<product> get(string uri)
        {
            try
            {
                string info = await uri.getAsync();
                return XmlHeplers.Deserialize<product>(info);
            }
            catch
            {
                throw;
            }
        }
    }
}
