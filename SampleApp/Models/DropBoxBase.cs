﻿using Dropbox.Api;
using SampleApp.ExtensionMethods;
using SampleApp.Infra.Models;
using System;
using System.Windows;
using System.Net.Http;
using System.Configuration;
using System.Threading.Tasks;

namespace SampleApp.Models
{
    public class DropBoxBase
    {
        private DropboxClient DbClient;
        private string oauth2State;
        private string RedirectUri = ConfigurationManager.AppSettings["RedirectUri"];

        public DropBoxBase(string ApiKey, string ApiSecret, string ApplicationName = "SampleApp")
        {
            try
            {
                AppKey = ApiKey;
                AppSecret = ApiSecret;
                AppName = ApplicationName;
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        public string GeneratedAuthenticationURL()
        {
            try
            {
                if (string.IsNullOrEmpty(RedirectUri))
                {
                    throw new Exception("RedirectUri can not be null");
                }
                this.oauth2State = Guid.NewGuid().ToString("N");
                Uri authorizeUri = DropboxOAuth2Helper.GetAuthorizeUri(OAuthResponseType.Token, AppKey, RedirectUri, state: oauth2State);
                AuthenticationURL = authorizeUri.AbsoluteUri.ToString();
                return authorizeUri.AbsoluteUri.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public string GenerateAccessToken()
        {
            try
            {
                string _accessToken = string.Empty;
                if (AppKey.CanAuthenticateToAppKey() && AppSecret.CanAuthenticateToAppKey())
                {
                    if (string.IsNullOrEmpty(AuthenticationURL))
                    {
                        throw new Exception("AuthenticationURL is not generated !");

                    }
                    Login login = new Login(AppKey, AuthenticationURL, this.oauth2State); // WPF window with Webbrowser control to redirect user for Dropbox login process.  
                    login.Owner = Application.Current.MainWindow;
                    login.ShowDialog();
                    if (login.Result)
                    {
                        _accessToken = login.AccessToken;
                        AccessTocken = login.AccessToken;
                        Uid = login.Uid;
                        DropboxClientConfig CC = new DropboxClientConfig(AppName, 1);
                        HttpClient HTC = new HttpClient();
                        HTC.Timeout = TimeSpan.FromMinutes(10); // set timeout for each ghttp request to Dropbox API.  
                        CC.HttpClient = HTC;
                        DbClient = new DropboxClient(AccessTocken, CC);
                    }
                    else
                    {
                        DbClient = null;
                        AccessTocken = string.Empty;
                        Uid = string.Empty;
                    }
                }

                return _accessToken;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public async Task<CurrentAccountInfo> GetCurrentAccount(string _accessToken)
        {
            try
            {
                DropboxClient client = new DropboxClient(_accessToken);

                var full = await client.Users.GetCurrentAccountAsync();

                CurrentAccountInfo currentAccountInfo = new CurrentAccountInfo();
                currentAccountInfo.Name = full.Name.DisplayName;
                currentAccountInfo.Email = full.Email;
                currentAccountInfo.PhotoUrl = full.ProfilePhotoUrl;

                return currentAccountInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Logout()
        {
            DbClient = null;
            AccessTocken = string.Empty;
            Uid = string.Empty;
        }

        public string AppName
        {
            get; private set;
        }
        public string AuthenticationURL
        {
            get; private set;
        }
        public string AppKey
        {
            get; private set;
        }
        public string AppSecret
        {
            get; private set;
        }
        public string AccessTocken
        {
            get; private set;
        }
        public string Uid
        {
            get; private set;
        }
    }
}
