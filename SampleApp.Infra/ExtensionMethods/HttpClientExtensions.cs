﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using SampleApp.Infra.Component;

namespace SampleApp.Infra.ExtensionMethods
{
    public static class HttpClientExtensions
    {
        public static async Task<string> getAsync(this String uri)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.Timeout = TimeSpan.FromSeconds(30);

                    GetAsyncMethod getAsyncMethod = new GetAsyncMethod();
                    return await getAsyncMethod.clientGetAsync(client, uri);
                }
                catch
                {
                    throw;
                }
            }
        }
    }
}