﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleApp.Infra.Models
{
    public class CurrentAccountInfo
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhotoUrl { get; set; }
    }
}
