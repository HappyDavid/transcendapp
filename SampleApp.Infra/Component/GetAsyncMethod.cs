﻿using System.Net.Http;
using System.Threading.Tasks;

namespace SampleApp.Infra.Component
{
    public class GetAsyncMethod
    {
        public async Task<string> clientGetAsync(HttpClient client, string uri)
        {
            HttpResponseMessage response = await client.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }
    }
}
